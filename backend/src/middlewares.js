export const errorhandler = (err, _req, res, next) => {
    if (err.name === "parameterError"){
        res.status(400).send("Invalid parameters");
    } else if (err.name === "databaseError"){
        res.status(400).send("Database request failed");
    } else {
        res.status(400).send(err.name);
    }
};

export const unknownEndpoint = (_req, res, _next) => {
    res.status(404).send('Unkown endpoint');
};
