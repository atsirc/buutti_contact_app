import 'dotenv/config';
import pg from "pg";
import { createContactsTable } from "./queries.js";

const isProd = process.env.NODE_ENV === "prod";

const {
    CONTACT_APP_PG_HOST, 
    CONTACT_APP_PG_USER, 
    CONTACT_APP_PG_DATABASE, 
    CONTACT_APP_PG_PASSWORD, 
    CONTACT_APP_PG_PORT
} = process.env;

export const pool = new pg.Pool({
    host: CONTACT_APP_PG_HOST,
    user: CONTACT_APP_PG_USER,
    database: CONTACT_APP_PG_DATABASE,
    password: CONTACT_APP_PG_PASSWORD,
    port: CONTACT_APP_PG_PORT,
    ssl: isProd
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        throw error;
    } finally{
        client.release();
    };
};

export const createTables = async () => {
    await Promise.all([
        await executeQuery(createContactsTable)
    ]);
    console.log("Tables created");
};

