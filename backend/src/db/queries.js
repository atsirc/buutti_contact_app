export const createContactsTable = `CREATE TABLE IF NOT EXISTS contacts (
	id serial PRIMARY KEY,
	firstname VARCHAR (50) NOT NULL,
	lastname VARCHAR (50) NOT NULL,
	address VARCHAR (100),
	email VARCHAR (50),
	phone VARCHAR (20),
	notes VARCHAR (200)

);`