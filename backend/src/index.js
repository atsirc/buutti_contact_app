import express from 'express';
import 'express-async-errors';
import contactsRouter from './routers/contactRouter.js';
import { createTables } from './db/db.js';
import { errorhandler, unknownEndpoint } from './middlewares.js';

const app = express();
app.use(express.json());

const environment = process.env.NODE_ENV;

process.env.NODE_ENV !== "test" && createTables();

app.use(express.json());
app.use('/contacts', contactsRouter);
app.use(express.static('build'));

app.use(errorhandler);
app.use(unknownEndpoint);

const port = process.env.CONTACT_APP_PORT;
environment !== 'test' && app.listen(port, () =>{
  console.log('Running on port ' +  port);
});

export default app;
