import contactsDao from "../dao/contactsDao.js";

// Aika kyseenalainen, en tiedä kannattaako pitää kiinni validaatiosta, vai ollanko tyytyväisiä siihen, että kentät on täytetty?
/*const validateContact = (req, _res, next) => {
  const body = req.body;
  if (body.firstname && body.lastname && body.firstname.length > 0 && body.lastname.length > 0) {
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const phoneRegex = /^\+?\d{5,12}$/;
    let counter = 0;
    if (body.address && body.address.length > 0) counter++;
    if (body.email && body.email.length > 0 && body.email.match(emailRegex)) {
      counter++;
    } else {
      body.email = '';
    }
    body.phone = body.phone.replace(' ', '') || '';
    if (body.phone && body.phone.length > 0 && body.phone.match(phoneRegex)) {
      counter++;
    } else {
      body.phone = '';
    }
    if (body.notes && body.notes.length > 0) counter++;
    if (counter >= 1) {
      req.body = body;
      return next();
    }
  }
  // TODO add which were incorrect
  throw new Error('some of the values were incorrect');
};*/

const checkRequiredParams = (newInfo) => {
    if (newInfo.firstname.length > 0 && newInfo.lastname.length > 0) {
        return true;
    } else {
        const err = new Error('Required fields are missing');
        err.name = 'fieldError';
        throw err;
    }
}

const checkParameterCount = (newInfo, expectedAmount) => {
    if (newInfo.id) expectedAmount++;
    if (Object.keys(newInfo).length === expectedAmount){
        return true;
    } else{
        const err = new Error("Invalid amount of parameters to send");
        err.name = "parameterError";
        throw err;
    };
};

// ei maailman parasta koodia...
const validateContact = (newInfo) => {
    const expectedAmount = 6;
    return checkParameterCount(newInfo, expectedAmount) &&
    checkRequiredParams(newInfo);

};

const getContact = async (id) => {
    const result = await contactsDao.getContact(id);
    return result;
}

const getAllContacts = async () => {
    const result = await contactsDao.getAllContacts();
    return result;
};

const addContact = async (newContact) => {
    try {
      if(validateContact(newContact)){
          const result = await contactsDao.addContact(newContact);
          return result;
      }
    } catch (error) {
      throw error
    }
};

const updateContact = async (updatedContact, id) => {
    try {
        if (validateContact(updatedContact)) {
           const result = await contactsDao.updateContact(updatedContact, id);
           console.log('update result', result)
           return result;
        }
    } catch (error) {
        throw error;
    }
}

const deleteContact = async (id) => {
  try {
    const contact = await contactsDao.deleteContact(id);
    return contact;
  } catch (error) {
    throw error;
  }
};

export default { getContact, getAllContacts, addContact, deleteContact, updateContact };
