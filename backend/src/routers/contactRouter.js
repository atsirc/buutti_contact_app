import express from 'express';
import contactServices from '../services/contactServices.js';

const router = express.Router();

router.get('/', async (_req, res) => {
    const contacts = await contactServices.getAllContacts(); //<---- muutin daon serviceksi
    res.status(200).json(contacts);
});

router.get('/:id', async (req, res) => {
    const id = Number(req.params.id);
    const contact = await contactServices.getContact(id);
    res.status(200).json(contact);
})

router.post('/', router.post('/', async (req, res) => {
  const result = await contactServices.addContact(req.body);
  console.log('post result', result);
  res.status(201).json(result)
}));

router.put('/:id', async (req, res) => {
    const id = Number(req.params.id)
    console.log(req.body)
    const result = await contactServices.updateContact(req.body, id);
    console.log('put result', result);
    res.status(200).json(result)
});

router.delete("/:id", async (req, res) => {
    const id = Number(req.params.id);
    const contact = await contactServices.deleteContact(id);
    res.status(200).json(contact);
});

export default router;
