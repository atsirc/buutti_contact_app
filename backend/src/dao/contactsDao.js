import { executeQuery } from "../db/db.js";

const handleError = (response) => {
  if (typeof response === 'undefined') {
    const err = new Error("Something went wrong with the database request");
    err.name = "databaseError";
    throw err;
  } else {
    return response;
  }
};

const addContact = async (contact) => {
    const newContact = [...Object.values(contact)];
    const result = await executeQuery(
        `INSERT INTO contacts (firstname, lastname, address, email, phone, notes)
        VALUES ($1, $2, $3, $4, $5, $6) RETURNING *;`,
        newContact
    );
    console.log(`${newContact[0]} added`);
    return handleError(result.rows[0]);
};

const getContact = async(id) => {
    const params = [id];
    const result = await executeQuery(
        'SELECT * FROM contacts WHERE id=$1;',
        params
    );
    return handleError(result.rows[0]);
};

const getAllContacts = async () => {
    const result = await executeQuery(
        'SELECT * FROM contacts ORDER BY id;'
    );
    console.log(`${result.rows.length} contacts found`);
    return handleError(result.rows);
};

const updateContact = async (contact, id) => {
    const params = [...Object.values(contact), id];
    params.shift();
    const result = await executeQuery(
        `UPDATE contacts 
        SET firstname = $1, 
            lastname = $2,
            address = $3,
            email = $4,
            phone = $5,
            notes = $6
        WHERE id = $7
        RETURNING *`,
        params
    );
    return handleError(result.rows[0]);
};

const deleteContact = async (id) => {
    const params = [id];
    const result = await executeQuery(
        `DELETE FROM contacts
        WHERE id = $1
        RETURNING *;`,
        params
    );
    return handleError(result.rows[0]);
};

export default { getContact, addContact, getAllContacts, updateContact, deleteContact };
