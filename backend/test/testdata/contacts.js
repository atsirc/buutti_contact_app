const contacts = [
  {
    id:1,
    firstname: 'Minna',
    lastname: 'Kukkonnen',
    address: 'Kotiosoite 4 B',
    email: 'minna.kukkonen@email.fi', 
    phone: '0501019870',
    notes: 'hyvä tyyppi'
  },
  {
    id: 2,
    firstname: 'Jarkko',
    lastname: 'Ihminen',
    address: 'Katuosoite 65 h 8',
    email: 'jarkko.ihminen@email.fi', 
    phone: '1115659375',
    notes: ''
  },
  {
    id: 3,
    firstname: 'Hanna',
    lastname: 'Vihaninen',
    address: 'Kaupunginkatu 5',
    email: 'hanna.vihaninen@shotmail.com', 
    phone: '1234565555',
    notes: 'Kannattaa varoa'
  },
  {
    id: 4,
    firstname: 'Paavo',
    lastname: 'Pesusieni',
    address: 'merikatu 679',
    email: '', 
    phone: '1112223333',
    notes: 'Tällane mias'
  },
];

export default contacts;
