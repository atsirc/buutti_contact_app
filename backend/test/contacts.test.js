import {jest} from '@jest/globals';
import request from 'supertest';
import app from '../src/index.js';
import {pool} from '../src/db/db.js';
import contacts from './testdata/contacts.js';

const initMock = (mockFunc) => {
  pool.connect = jest.fn(()=>{
    return {
      query: mockFunc,
      release: () => null
    };
  });
}

describe('GET contacts', () => {

  it('get all contacts returns 200 and response matches test response', async () => {
    const mockResponse = {
      rows: [...contacts]
    };

    initMock(() => mockResponse);

    const response = await request(app).get('/contacts/');

    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
  });

  it('get contact with id 1 returns contact that matches object', async () => {
    const mockResponse = {
      rows: [contacts[0]]
    };

    initMock(() => mockResponse);

    const response = await request(app).get('/contacts/' + contacts[0].id);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject(contacts[0]);
  });

  it('get contact with id 10 that does not exist returns 400', async () => {
    const mockResponse = {
      rows: []
    };

    initMock(() => mockResponse);

    const response = await request(app).get('/contacts/' + 10);

    expect(response.status).toBe(400);
  });
});


describe('POST contact', () => {

  it('posting new contact returns 201', async () => {
    const query = {
      firstname: 'Minna',
      lastname: 'Kukkonnen',
      address: 'Kotiosoite 4 B',
      email: 'minna.kukkonen@email.fi', 
      phone: '0501019870',
      notes: 'hyvä tyyppi'
    };

    const mockResponse = {
      rows: [query],
    };
  
    initMock(() => mockResponse);

    const response = await request(app)
      .post('/contacts/')
      .send(query);

    expect(response.status).toBe(201);
  });

  it("adding a contact without all parameters returns 400", async () => {
    const query = {
      firstname: 'Minna',
      lastname: 'Kukkonnen',
      email: 'minna.kukkonen@email.fi', 
      phone: '0501019870',
      notes: 'hyvä tyyppi'
    };
  

  const mockResponse = {
    rows: [query],
  };

  initMock(() => mockResponse);

  const response = await request(app)
      .post('/contacts/')
      .send(query);

  expect(response.status).toBe(400);
  expect("Invalid parameters");
  });
});

describe('PUT contact', () => {

  it('updating first contact returns updated contact', async () => {
    const mockResponse = {
      rows: [{...contacts[0], notes: 'unpopular'}]
    };

    initMock(() => mockResponse);

    const updatedContact = {
      ...contacts[0],
      notes: 'unpopular'
    }

    const response = await request(app)
      .put('/contacts/'+ contacts[0].id)
      .send(updatedContact);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject(updatedContact);
  });

  it('if updated post has no names returns 400', async () => {

    const updatedContact = {
      firstname: '',
      lastname: '',
      address: '',
      email: 'slkdjlkfsd',
      phone: 'isdf',
      notes: ''
    }

    const response = await request(app)
      .put('/contacts/'+ contacts[0].id)
      .send(updatedContact);

    expect(response.status).toBe(400);
    //TODO add message equals
  });

});

describe('DELETE contact', () => {

  it('return contact with id ' + contacts[0].id + ' returns 200', async () => {
    const mockResponse = {
      rows: [contacts[0]]
    };

    initMock(() => mockResponse);

    const response = await request(app).delete('/contacts/' + contacts[0].id);
    expect(response.status).toBe(200);
    expect(response.body).toMatchObject(contacts[0]);
  });

  it('deleting contact with non-existing id returns 400', async () => {
    const mockResponse = {
      rows: []
    };

    initMock(() => mockResponse);

    const response = await request(app).delete('/contacts/' + 10);
    expect(response.status).toBe(400);
    //TODO add error message
  });
});
