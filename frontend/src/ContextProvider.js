import { useEffect, useState, createContext } from 'react';
import { getAll } from './services/ContactService';
// import { useEffect, useState, createContext } from 'react';

export const AppContext = createContext([{}, () => {}]); // Create context



const ContextProvider = props => { // Provider provides child components with props
    const [state, setState] = useState({});

    useEffect(() => {
        // console.log("käydään täällä");
        const getInitialContacts = async () => {
            const initialContent = await getAll()
            setState(() => {
                return {contacts:initialContent};
            });
        }
        getInitialContacts();
    },[]);
    
    //useEffect(() => {
        //setState(state => {
            //return {
                //contacts: null,
                //message: '',
                //chosenContact: null
            //}
        //})
    //}, []);

    return (
        <AppContext.Provider value={[state, setState]}>
            {state.contacts && props.children}
        </AppContext.Provider>
    );

};

export default ContextProvider
