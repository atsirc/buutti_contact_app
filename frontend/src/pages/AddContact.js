import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { addOne } from "../services/ContactService.js";
import { AppContext } from "../ContextProvider";

const AddContact = () => {
  const [state, setState] = useContext(AppContext);
  const navigate = useNavigate();
  const [error, setError] = useState(false)

  const validateForm = (contact) => {
    const {firstname, lastname, address, email, phone, notes} = contact;

    if ([address, email, phone, notes].join('') === '') {
      setError(state => {
        return true;
      });
      return false;
    } else {
      return true;
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const contact = {
      firstname: event.target.firstname.value,
      lastname: event.target.lastname.value,
      address: event.target.address.value,
      email: event.target.email.value,
      phone: event.target.phone.value,
      notes: event.target.notes.value,
    };

    if (validateForm(contact)) {
      const contactFromBackend = await addOne(contact);

      contactFromBackend !== null
        ? setState((state) => {
            const updatedContacts = [...state.contacts, contactFromBackend];
            const successMessage = `${contactFromBackend.firstname} ${contactFromBackend.lastname} was added successfully!`
            return { ...state, contacts: updatedContacts, message: successMessage};
          })
        : setState((state) => {
            return {
              ...state, message: 'An error occuerd while adding contact'
            }
        });

      navigate('/');
    }
  };

  return (
    <div className="container">
      <h2>Add a new contact</h2>
      { error &&
        <p className="error-message">
          Firstname and lastname are required. Additionally one of the following fields: address, email, phone or notes needs to be filled.
        </p>
      }
      <form onSubmit={(e) => handleSubmit(e)}>
        <label className="form" for="firstname">
          First name:{" "}
        </label>
        <input
          className="perus-input"
          type="text"
          name="firstname"
          required
        ></input>

        <label className="form" for="lastname">
          Last name:{" "}
        </label>
        <input
          className="perus-input"
          type="text"
          name="lastname"
          required
        ></input>

        <label className="form" for="address">
          Address:{" "}
        </label>
        <input className="perus-input" type="text" name="address"></input>

        <label className="form" for="email">
          Email:{" "}
        </label>
        <input className="perus-input" type="text" name="email"></input>

        <label className="form" for="phone">
          Phone:{" "}
        </label>
        <input className="perus-input" type="text" name="phone"></input>

        <label className="form " for="notes">
          Notes:{" "}
        </label>
        <textarea
          className="perus-input isompi"
          type="textarea"
          name="notes"
        ></textarea>
        <button className="perus-btn" type="submit">
          Add
        </button>
      </form>
    </div>
  );
};

export default AddContact;
