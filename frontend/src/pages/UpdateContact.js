import { useContext, useState } from 'react';
import { AppContext } from '../ContextProvider';
import { useNavigate} from "react-router-dom";
import { UpdateOne } from '../services/ContactService';

const UpdateContact = () => {
    const navigate = useNavigate();
    const [state, setState] = useContext(AppContext);
    const [error, setError] = useState(false);

    const validateForm = (contact) => {
         const {firstname, lastname, address, email, phone, notes} = contact;

         if ([address, email, phone, notes].join('') === '') {
              setError(state => {
                 return true;
              });
              return false;
         } else {
              return true;
         }
    };

    const handleUpdate = async (event) => {
        event.preventDefault();

        const updatedContact = {
            id: state.chosenContact.id,
            firstname: event.target.firstname.value,
            lastname: event.target.lastname.value,
            address: event.target.address.value,
            email: event.target.email.value,
            phone: event.target.phone.value,
            notes: event.target.notes.value
        };

        if (validateForm(updatedContact)) {
            const status = await UpdateOne(updatedContact);
            if (status === 200) {
                setState((state) => {
                    return {
                        ...state,
                        contacts: state.contacts.map((contact) => contact.id === updatedContact.id ? updatedContact : contact),
                        chosenContact: updatedContact,
                        message: `${updatedContact.firstname} ${updatedContact.lastname}'s information was successfully updated!`
                    }
                });

                navigate(`/${updatedContact.id}`)
            } else {
                setState((state) => {
                    return {
                        ...state,
                        message: `An error occured while updating ${updatedContact.firstname} ${updatedContact.lastname}`
                    }
                });
            }
        }
    }

    return(
        <div className="container">
            <h2>Update a contact</h2>
            { error &&
                <p className="error-message">
                  Firstname and lastname are required. Additionally one of the following fields: address, email, phone or notes needs to be filled.
                </p>
            }
                <form onSubmit={e => handleUpdate(e)}>
                    <label className="form" for="firstname">Firstname</label>
                    <input className="perus-input" defaultValue={state.chosenContact.firstname} type="text" name="firstname" required></input>

                    <label className="form" for="lastname">Lastname</label>
                    <input className="perus-input" defaultValue={state.chosenContact.lastname} type="text" name="lastname" required></input>

                    <label className="form" for="address">Address</label>
                    <input className="perus-input" defaultValue={state.chosenContact.address} type="text" name="address"></input>

                    <label className="form" for="email">Email</label>
                    <input className="perus-input" defaultValue={state.chosenContact.email} type="text" name="email"></input>

                    <label className="form" for="phone">Phone</label>
                    <input className="perus-input" defaultValue={state.chosenContact.phone} type="text" name="phone"></input>

                    <label className="form" for="notes">Notes</label>
                    <input className="perus-input" defaultValue={state.chosenContact.notes} type="text" name="notes"></input>

                    <button className="perus-btn" type='submit'>Submit update</button>

                </form>
        </div>
    )
};

export default UpdateContact;
