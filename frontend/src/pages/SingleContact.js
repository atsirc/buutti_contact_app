import { useEffect, useContext, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { AppContext } from "../ContextProvider";
import { removeOne } from "../services/ContactService.js";

const SingleContact = () => {
  const navigate = useNavigate();
  const params = useParams();
  const id = params.id;
  const [state, setState] = useContext(AppContext);

  // Näyttää toimivan ilman mutta jätän tähän vielä väliaikasesti
  // const getChosenContact = async () => {
  //   return state.contacts.find(contact => contact.id === Number(id));
  // };

  useEffect(() => {  
    const checkChosenContact = () => {
      if(state.contacts){
        const tempContact = state.contacts.find(contact => contact.id === Number(id));
        setState((state) => {
          return {...state, chosenContact: tempContact } 
        });
      };
    };
    checkChosenContact()
  },[]);

  const handleRemove = async () => {
    try {
      const status = await removeOne(id);
      status === 200 && setState((state) => {
          return {
            ...state,
            contacts: [...state.contacts.filter((contact) => contact.id !== Number(id))],
            message: `${state.chosenContact.firstname} ${state.chosenContact.lastname} was deleted successfully`
          };
        });
      navigate('/');
    } catch (error) {
      setState(state => {
          return {
              ...state,
                message: `An error occured ${error.message}`
          };
      });
    }
  };

    
    const handleUpdate = async () => {
        navigate('/update')
    }
    



  return (
    <div className="container">
      {state.chosenContact && 
        <div className="tiedot">
          <h2 className="otsikko marginpois">
            Contact #{state.chosenContact.id}
          </h2>

          {/* <p className="tieto"><p className="tieto2">Id:</p> {state.chosenContact.id}</p> */}
          <p className="tieto">
            <span className="tieto2">First name: </span>
            {state.chosenContact.firstname}
          </p>
          <p className="tieto">
            <span className="tieto2">Last name: </span>
            {state.chosenContact.lastname}
          </p>
          <p className="tieto">
            <span className="tieto2">Address: </span>
            {state.chosenContact.address}
          </p>
          <p className="tieto">
            <span className="tieto2">Email: </span>
            {state.chosenContact.email}
          </p>
          <p className="tieto">
            <span className="tieto2">Phone: </span>
            {state.chosenContact.phone}
          </p>
          <p className="tieto lyhyempi">
            <span className="tieto2">Notes: </span>
            {state.chosenContact.notes}
          </p>

          <div className="napit">
            <button className="perus-btn delete" onClick={(e) => handleRemove(e)}>
              Delete
            </button>
            <button className="perus-btn update" onClick={() => handleUpdate()}>
              Update
            </button>
          </div>
        </div>
      }
    </div>
  );
};

export default SingleContact
