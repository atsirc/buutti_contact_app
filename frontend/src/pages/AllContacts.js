import { useContext, useState } from 'react'
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { AppContext } from '../ContextProvider';
// import { getAll } from '../services/ContactService.js'

const AllContacts = () => {
    const navigate = useNavigate()
    const [state, setState] = useContext(AppContext);
    const [search, setSearch] = useState('');
    const [searchResult, setSearchResult] = useState([]);

    const handleChange = ({target}) => {
        switch (target.name) {
            case 'search': 
                setSearch(target.value); 
                performSearch(target.value); 
                break;
            default: break;
        }
    }

    // ottaa parametrin koska muuten se käyttää edeltävää search-valueta
    const performSearch = (search) => {
        // case insensitive search, hakee jokaisesta kentästä
        const regex = new RegExp(`${search}`, 'gui');
        const result = state.contacts.filter((contact) => {
            // tehdään stringi kaikista valuesista ja etistään niistä.
            const string = Object.values(contact).join(' ').toLowerCase();
            if (string.match(regex) !== null) {
                return contact;
            }
        });
        setSearchResult(result);
    };

    const emptySearch = () => {
        setSearchResult([]);
        setSearch('');
    }

    // //navigoi endpointiin jossa lisätään uusi tuote
    // const handleAdd = async (event) => {
    //     event.preventDefault();
    //     navigate('/add')
    // }


    // Tänne tulee vielä search kontaktilistan alle

    //navigoi yhden kontaktin näkymään
    const handleContact = async (event, contact) => {
        event.preventDefault();
        setState((state) => {
            return {...state, chosenContact: contact}
        })
        navigate(`/${contact.id}`)
    }

    // Table ei ole ainakaan vielä järkevästi responsiivinen.

    return (
        <>
            
            <div className="container">

            <h2 className="otsikko">All contacts</h2>

            <div className="search">
                <label className="form search-form bold search-label" htmlFor="search">Search: </label>
                <input className="search-input" type="text" name="search" onChange={(ev) => {handleChange(ev)}} value={search}/>
                <button className="search-btn" onClick={emptySearch}>Empty search</button>
                <br></br>
            </div>
           
            

            <table className="taulu">
                <thead>
                    <tr>
                        <th className="id">ID</th>
                        <th className="lyhyt">First</th>
                        <th className="lyhyt">Last</th>
                        <th>Address</th>
                        <th className="email">Email</th>
                        <th>Phone</th>
                        <th className="notes">Notes</th>
                    </tr>
                </thead>
                <tbody>
                    {search.length !== 0 && searchResult.map((contact) => {
                        return(
                            <tr key={contact.id} onClick={(e) => handleContact(e, contact)}>
                                <td>{contact.id}</td>
                                <td>{contact.firstname}</td>
                                <td>{contact.lastname}</td>
                                <td>{contact.address}</td>
                                <td>{contact.email}</td>
                                <td>{contact.phone}</td>
                                <td>{contact.notes}</td>
                            </tr>
                        )
                    })}

                    {search.length === 0 && state.hasOwnProperty('contacts') && state.contacts.map((contact) => {
                        return(
                            <tr key={contact.id} onClick={(e) => handleContact(e, contact)}>
                                <td>{contact.id}</td>
                                <td>{contact.firstname}</td>
                                <td>{contact.lastname}</td>
                                <td>{contact.address}</td>
                                <td>{contact.email}</td>
                                <td>{contact.phone}</td>
                                <td>{contact.notes}</td>
                            </tr>

                            
                        )
                    
                    })}
             {/*
                <tbody table className="taulu">
                    {state.hasOwnProperty('contacts') && state.contacts.map((contact) => {
                    return(
                        <tr key={contact.id} onClick={(e) => handleContact(e, contact)}>
                            <td className="id">{contact.id}</td>
                            <td className="lyhyt">{contact.firstname}</td>
                            <td className="lyhyt">{contact.lastname}</td>
                            <td>{contact.address}</td>
                            <td className="email">{contact.email}</td>
                            <td>{contact.phone}</td>
                            <td className="notes">{contact.notes}</td>
                        </tr>
                    )
                })}
                */}
                </tbody>
             
            </table>

          
          
            {/* <div className="next container">
        <li>
          <Link to="/" className="next-nav">
            Previous 10
          </Link>
        </li>
        <li>
          <Link to="/" className="next-nav">
            Next 10
          </Link>
        </li>
      </div> */}
        </div>
        
        </>
        
    )
};


export default AllContacts
