import { useEffect, useContext, useState } from "react";
import { AppContext } from "./ContextProvider";

const Message = () => {
  const [state, setState] = useContext(AppContext);

  useEffect(() => {
    if (state.message && state.message.length > 0) {
      setTimeout(()=> {
        setState(state => {
          return {
            ...state,
            message: ''
          }
        })
      }, 5000);
    } 
  }, [state])

  return (
    <>
      {state.hasOwnProperty('message') && state.message.length > 0 ? 
        <div className={state.message.includes('error') ? 'message error' : 'message success'}>
          <p>{state.message}</p>
        </div> : ''}
    </>
  )
};

export default Message;
