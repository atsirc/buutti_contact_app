import { 
  Routes, 
  BrowserRouter as Router, 
  Route,
  Link
} from 'react-router-dom'

import "./style.css"
import './App.css';
import axios from 'axios';
import {useState, useEffect} from 'react';
import AllContacts from './pages/AllContacts'
import ContextProvider from './ContextProvider';
import AddContact from './pages/AddContact'
import SingleContact from './pages/SingleContact';
import Navbar from "./Navbar";
import Message from "./Message";
import UpdateContact from './pages/UpdateContact';

const App = () => {

  return (
    <ContextProvider>
      <Message />
      <div className="App container">
        <h1>Contact Bar</h1>
        <Router>
        <Navbar />
          <Routes>
            <Route path='/' element={<AllContacts/>}></Route>
            <Route path='/add' element={<AddContact/>}></Route>
            <Route path='/:id' element={<SingleContact/>}></Route>
            <Route path='/update' element={<UpdateContact/>}></Route>
          </Routes>
        </Router>
      </div>
    </ContextProvider>
  );
}


export default App;
