import axios from 'axios'

export const getAll = async () => {
    const response = await axios.get('/contacts')
    return response.data
}

export const removeOne = async (id) => {
    const response = await axios.delete(`/contacts/${id}`)
    console.log(response)
    return response.status
}

export const addOne = async (book) => {
    const response = await axios.post(`/contacts`, book)
    if (response.status === 201){
        return response.data}
    else return null
}

export const UpdateOne = async (contact) => {
    const response = await axios.put(`/contacts/${contact.id}`, contact)
    console.log(response)
    return response.status
}